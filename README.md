# fvtt-fragged-empire

This is a basic fragged empire system for foundry VTT. It is not an officially licensed system but contains images with with the express permission of Fragged's creator and rights holder Wade Dyer. Icons from game-icons.net have not been modified and have been under the following license: https://creativecommons.org/licenses/by/3.0/legalcode

It is set up to be primary an item based drag and drop system. You create an actor, you create your items, and you can drag and drop items to the actor. You can also drag and drop variations, traits, perks, modifications onto weapons, actors, and other features.

I will do a proper documentation write up, or a tutorial video in the near future.

For now, if you have any questions please ask them on the VTT channel of the Fragged discord server or private message me on discord (DedicatedDM#8004)

If you have any bugs or issues to be raised, please run them passed me and if verified I will log them an issue here to be tacked and improved.

Thanks,

R
